﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chronicle
{
    public interface ITableScript
    {
        void Parse(CEParser.File f);
    }

    //[Serializable]
    //public class ScriptFactory : MarshalByRefObject
    //{
    //    public ScriptFactory() { }

    //    public ITableScript Create(string assemblyFile, string typeName, object[] constructArgs)
    //    {
    //        return (ITableScript)Activator.CreateInstance(assemblyFile, typeName, constructArgs);
    //    }

    //}
}
